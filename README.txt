This module imports Advogato diary entries to a Drupal website
during cron runs with the cron.php file in the root directory.

You must configure the administrative settings before users can queue
diary entries to be imported.

Each cron run is set at a low default number because sometimes Advogato
can be bogged down and will cause cron runs to timeout. Since Drupal
doesn't really support MySQL transactional DB types, timing out can be
a bad thing for keeping track of what has been imported and not.

INSTALLATION
1. put the advogato_import directory to your modules directory
2. activate the module at "Administer > Site Building > Modules"
3. configure the module at "Administer > Site configuration > Advogato import"
4. users can then queue node imports in "My account > advogato_import"

AUTHOR
David Kent Norman
http://deekayen.net/

ADDITIONAL NOTES
I created this script for a one-time run of importing my own diary.
I don't intend to provide any more development on this module,
so if you're a developer itching to make a change, go ahead.

To delete the entries from Advogato after you import them, I copied
Drupal's xmlrpc.php file, put the following to the top it, and ran it
at a command prompt with `php -q xmlrpc.php`. It is not implemented
in the automated part of this module because of the obvious data
destruction if something goes wrong.

===========================================================

<?php
set_time_limit(0);
$username = 'your_username';
$password = 'my_password';
$len = (int)xmlrpc('http://www.advogato.org/XMLRPC', 'diary.len', $username);
$cookie = xmlrpc('http://www.advogato.org/XMLRPC', 'authenticate', $username, $password);
for ($i = 0; $i <= $len; $i++) {
  echo xmlrpc('http://www.advogato.org/XMLRPC', 'diary.set', $cookie, $i, ' ');
}