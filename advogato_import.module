<?php

/**
 * @file
 * Imports diary entries to Drupal from Advogato.org XMLRPC
 *
 * @author David Kent Norman
 * @link http://deekayen.net/
 * @link http://www.advogato.org/xmlrpc.html
 * @copyright Copyright 2006, David Kent Norman
 * @todo make option to delete diary entries from Advogato after imported to Drupal
 */

/**
 * Implementation of hook_menu().
 *
 * Displays a tab in users' account area.
 *
 * @return array
 */
function advogato_import_menu() {
  $items = array();

  $items['admin/settings/advogato_import'] = array(
    'title' => 'Advogato import',
    'description' => 'Set default import locations, settings, and limits.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advogato_import_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM
  );
  $items['user/%user/advogato_import'] = array(
    'title' => 'Advogato import',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advogato_import_diary_user', 'user'),
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'variable_get',
    'access arguments' => array('advogato_import_configured', FALSE),
    'weight' => 2
  );

  return $items;
}

/**
 * Administration variables must be set before users can queue imports
 *
 * @return array
 */
function advogato_import_admin_settings() {
  $form = array();
  $form['advogato_import_cron_limit'] = array(
    '#type' => 'select',
    '#title' => t('Import limit'),
    '#default_value' => variable_get('advogato_import_cron_limit', 15),
    '#options' => drupal_map_assoc(array(0, 15, 30, 50, 100, 250, 500)),
    '#description' => t('The maximum number of diary entries to import from Advogato in a single cron run. Set to 0 for unlimited imports. Set this lower if your cron is timing out or if PHP is running out of memory.')
  );
  $form['advogato_import_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Default node type'),
    '#default_value' => variable_get('advogato_import_node_type', 'blog'),
    '#options' => drupal_map_assoc(array('blog', 'page', 'story')),
    '#description' => t('This module has not checked to see if the options are enabled in the modules control panel. Please verify the node type you select is a node type your site supports.')
  );

  $form['default_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default options')
  );
  $form['default_options']['advogato_import_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => variable_get('advogato_import_status', 1)
  );
  $form['default_options']['advogato_import_moderation_queue'] = array(
    '#type' => 'checkbox',
    '#title' => t('In moderation queue'),
    '#default_value' => variable_get('advogato_import_moderation_queue', 0)
  );
  $form['default_options']['advogato_import_promoted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Promoted to front page'),
    '#default_value' => variable_get('advogato_import_promoted', 0)
  );
  $form['default_options']['advogato_import_sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sticky at top of lists'),
    '#default_value' => variable_get('advogato_import_sticky', 0)
  );

  $form['user_comments'] = array(
    '#type' => 'fieldset',
    '#title' => t('User comments')
  );
  $form['user_comments']['advogato_import_comment'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('advogato_import_comment', 0),
    '#options' => array(0 => t('Disabled'), 1 => t('Read only'), 2 => t('Read/write'))
  );

/* not really sure how/if this should be implemented - DKN
  $form['advogato_import_format'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('advogato_import_format', 0),
    '#options' => array(0 => t('Filtered HTML'), 1 => t('PHP code'), 2 => t('Full HTML'))
  );
*/
  $form['advogato_import_xmlrpc'] = array(
    '#type' => 'textfield',
    '#title' => t('Advogato XMLRPC interface'),
    '#default_value' => variable_get('advogato_import_xmlrpc', 'http://www.advogato.org/XMLRPC'),
    '#size' => 70,
    '#maxlength' => 255,
    '#description' => t('Location of Advogato\'s XMLRPC interface. This should not need changing, but if Advogato changes, this should make it so you don\'t have to edit the module source.')
  );
  $form['advogato_import_configured'] = array(
    '#type' => 'hidden',
    '#value' => 1
  );
  return system_settings_form($form);
}

/**
 * Accepts username, and diary entry numbers to the import queue
 */
function advogato_import_diary_user($user) {
  $edit = array(
    'username' => '',
    'startentry' => '',
    'endentry' => ''
  );
  $result = db_query('SELECT username, startentry, endentry FROM {advogato_import} WHERE uid = %d', $user->uid);
  while ($record = db_fetch_array($result)) {
    if (!empty($record['username'])) {
      $edit = $record;
    }
  }
  unset($record, $result);

  $form = array();
  $form['advogato_import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Advogato diary'),
    '#description' => t('Queue import of diary entries from Advogato.org. Entries will be added in chunks during cron jobs. Advogato diary entries are numbered starting at 0 (zero).')
  );
  $form['advogato_import']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Advogato username'),
    '#default_value' => isset($edit['username']) ? $edit['username'] : '',
    '#size' => 20,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#weight' => 1
  );
  $form['advogato_import']['startentry'] = array(
    '#type' => 'textfield',
    '#title' => t('Start entry'),
    '#default_value' => $edit['startentry'],
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#weight' => 2
  );
  $form['advogato_import']['endentry'] = array(
    '#type' => 'textfield',
    '#title' => t('End entry'),
    '#default_value' => $edit['endentry'],
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#weight' => 3
  );
  $form['advogato_import']['queue_import'] = array(
    '#type' => 'submit',
    '#value' => t('Queue Import'),
    '#weight' => 20
  );
  return $form;
}

function advogato_import_diary_user_validate($form, &$form_status) {
  if (!$form_status['values']['username']) {
    form_set_error('username', t('You must enter a username.'));
  }
  if (!isset($form_status['values']['startentry']) || !is_numeric($form_status['values']['startentry'])) {
    form_set_error('startentry', t('You must enter a starting entry.'));
  }
  if (!isset($form_status['values']['endentry']) || !is_numeric($form_status['values']['endentry'])) {
    form_set_error('endentry', t('You must enter an ending entry.'));
  }
}

function advogato_import_diary_user_submit($form, &$form_status) {
  global $user;
  db_query("INSERT INTO {advogato_import} (uid, username, startentry, endentry) VALUES (%d, '%s', '%s', '%s')", $user->uid, $form_status['values']['username'], $form_status['values']['startentry'], $form_status['values']['endentry']);

  drupal_set_message(t('Your diary import request has been queued.'));

  drupal_goto("user/$user->uid");
}

/**
 * Implementation of hook_cron().
 *
 * This function could benefit from a transactional DB
 */
function advogato_import_cron() {
  if (variable_get('advogato_import_configured', 0)) {

    $result = db_result(db_query('SELECT COUNT(qid) FROM {advogato_import}'));
    if ($result > 0) {
      $loop_limit     = $cron_limit = (int)variable_get('advogato_import_cron_limit', 0);
      $xmlrpc_addr    = variable_get('advogato_import_xmlrpc', 'http://www.advogato.org/XMLRPC');
      $node->status   = variable_get('advogato_import_status', 0); // 1 = published
      $node->type     = variable_get('advogato_import_node_type', 'blog');
      $node->moderate = variable_get('advogato_import_moderation_queue', 0);
      $node->promote  = variable_get('advogato_import_promoted', 0);
      $node->sticky   = variable_get('advogato_import_sticky', 0);
      $node->comment  = variable_get('advogato_import_comment', 0); // 0 = disabled
      $node->format   = 3; // Drupal's default for Full HTML; Advogato already did filtering
      $node->files    = array();

      $result = db_query('SELECT qid, uid, username, startentry, endentry, ABS(endentry - startentry) AS difference FROM {advogato_import}');
      while ($queue = db_fetch_object($result)) {
        $account    = user_load(array('uid' => $queue->uid));
        $node->name = $account->name;
        $node->uid  = $queue->uid;

        $queue->startentry = (int)$queue->startentry; // xmlrpc is picky about getting strings where it wants integers

        $abs = (int)$queue->difference;
        $asc = $queue->startentry < $queue->endentry ? TRUE : FALSE;
        do {
          // fetch entry
          $entry = xmlrpc($xmlrpc_addr, 'diary.get', $queue->username, $queue->startentry);
          $date = xmlrpc($xmlrpc_addr, 'diary.getDates', $queue->username, $queue->startentry);

          // assign entry vars
          $node->created = mktime($date[0]->hour, $date[0]->minute, $date[0]->second, $date[0]->month, $date[0]->day, $date[0]->year);
          $node->changed = mktime($date[1]->hour, $date[1]->minute, $date[1]->second, $date[1]->month, $date[1]->day, $date[1]->year);
          $node->title   = strftime("%d %b %Y", $node->created);
          $node->teaser  = $node->body = str_replace("\n", ' ', $entry); // Drupal does nl2br by default

          // write to DB
          unset($node->nid, $node->vid); // if nid isn't reset, node_save will just overwrite the previous loop's save to the old nid

          // node_submit() would probably be more "proper", but it calls
          // drupal_set_message() on each execution, which can be messy
          // on users' next page load with lots of imports.
          node_save($node);

          // node_submit() normally calls this
          watchdog('content', '%type: added %title.',
            array('%type' => $node->type, '%title' => $node->title),
            WATCHDOG_INFO, l('view', "node/$node->nid"));

          // update queue
          $asc === TRUE ? $queue->startentry++ : $queue->startentry--;
          db_query('UPDATE {advogato_import} SET startentry = %d WHERE qid = %d', $queue->startentry, $queue->qid);
          $abs--;
          $loop_limit--;
        } while (($cron_limit === 0 xor $loop_limit > 0) && $abs >= 0);

        if (($asc === TRUE && $queue->startentry > $queue->endentry) || ($asc === FALSE && $queue->startentry < $queue->endentry)) {
          db_query('DELETE FROM {advogato_import} WHERE qid = %d', $queue->qid);
        }
      }
    }
  }

  db_query('DELETE FROM {advogato_import} WHERE ABS(endentry - startentry) = 0');
}
